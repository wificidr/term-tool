extern crate pest;
#[macro_use]
extern crate pest_derive;
use pest::error::Error;
use pest::Parser;
use std::collections::HashMap;
use std::fs;
use std::hash::Hash;

#[derive(Parser)]
#[grammar = "junos.pest"]
pub struct JunosParser;

#[derive(Debug)]
pub enum JunosConfigError {
    CastingError,
}

#[derive(Debug)]
pub enum JunosConfig<'a> {
    ConfigMap(HashMap<String, Option<Box<JunosConfig<'a>>>>),
    Object(String, Option<Box<JunosConfig<'a>>>),
    QuotedString(&'a str),
    Array(Vec<&'a str>),
}

impl<'a> JunosConfig<'a> {
    pub fn as_map(&'a self) -> Result<&HashMap<String, Option<Box<Self>>>, JunosConfigError> {
        match self {
            JunosConfig::ConfigMap(m) => Ok(m),
            _ => Err(JunosConfigError::CastingError),
        }
    }
    pub fn as_key_vec(&'a self) -> Result<Vec<String>, JunosConfigError> {
        match self {
            JunosConfig::ConfigMap(m) => {
                let mut keys = Vec::with_capacity(m.len());
                for key in m.keys() {
                    keys.push(key.clone());
                }
                Ok(keys)
            }
            _ => Err(JunosConfigError::CastingError),
        }
    }
}

// Trivial load a file to text
pub fn load_file(fname: &str) -> String {
    fs::read_to_string(fname).expect("cannot read file")
}

// Turn tokens into a data structure
pub fn parse_file(file: &str) -> Result<JunosConfig, Error<Rule>> {
    let raw = JunosParser::parse(Rule::file, file)?.next().unwrap();
    use pest::iterators::Pair;

    fn parse_value(pair: Pair<Rule>) -> JunosConfig {
        let rule = pair.as_rule();
        // let deb = pair.clone();
        match rule {
            Rule::pair | Rule::expression => {
                let mut keywords: Vec<&str> = vec![];
                let mut value: Option<Box<JunosConfig>> = None;
                for inner_rule in pair.into_inner() {
                    match inner_rule.as_rule() {
                        Rule::keyword => keywords.push(inner_rule.as_str()),
                        Rule::value | Rule::term => value = Some(Box::new(parse_value(inner_rule))),
                        x => {
                            unreachable!("{:?} is unreachable!", x)
                        }
                    }
                }
                JunosConfig::Object(keywords.join(" "), value)
            }
            Rule::term => {
                let mut node = HashMap::new();
                for obj in pair
                    .into_inner()
                    .map(parse_value)
                    .collect::<Vec<JunosConfig>>()
                {
                    match obj {
                        JunosConfig::Object(key, val) => {
                            node.insert(key, val);
                        }
                        _ => unreachable!(),
                    }
                }
                JunosConfig::ConfigMap(node)
            }
            Rule::value => {
                let inner_rule = pair.into_inner().next().unwrap();
                match inner_rule.as_rule() {
                    Rule::string => JunosConfig::QuotedString(inner_rule.as_str()),
                    Rule::array => JunosConfig::Array(
                        inner_rule.into_inner().map(|pair| pair.as_str()).collect(),
                    ),
                    _ => unreachable!(),
                }
            }
            Rule::keyword
            | Rule::object
            | Rule::array
            | Rule::string
            | Rule::symbols
            | Rule::keychar
            | Rule::wildcard
            | Rule::file
            | Rule::inner
            | Rule::char
            | Rule::line_comment
            | Rule::multiline_comment
            | Rule::EOI
            | Rule::COMMENT
            | Rule::WHITESPACE => {
                unreachable!("{:?} is unreachable!", rule)
            }
        }
    }
    Ok(parse_value(raw))
}

pub fn get_map_node<'a>(
    config_tree: &'a JunosConfig,
    key_name: &str,
) -> Option<&'a Option<Box<JunosConfig<'a>>>> {
    match config_tree {
        JunosConfig::ConfigMap(map) => map.get(key_name),
        _ => unreachable!("root is wrong type"),
    }
}
