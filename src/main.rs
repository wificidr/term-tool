use std::collections::HashMap;
use std::env;
use std::net::IpAddr;

use term_tool::{get_map_node, load_file, parse_file, JunosConfig};

#[derive(Debug)]
struct ArgumentError;

fn main() -> Result<(), ArgumentError> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("please pass a filename");
        return Err(ArgumentError);
    }

    let text = load_file(&args[1]);
    let config_tree = parse_file(&text).unwrap();
    // blow up the screen
    println!("{:?}", config_tree);
    // TODO: write a proper lookup function!
    // let policy_config = get_map_node(&config_tree, "policy-options")
    //     .unwrap()
    //     .as_ref()
    //     .unwrap();
    Ok(())
}
