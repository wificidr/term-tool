use term_tool;

#[test]
fn test_parser_tiny() {
    let text = term_tool::load_file("tests/tiny.cfg");
    term_tool::parse_file(&text).unwrap();
}

/*
#[test]
fn test_parser_full() {
    let text = term_tool::load_file("tests/full.cfg");
    term_tool::parse_file(&text).unwrap();
}
*/
